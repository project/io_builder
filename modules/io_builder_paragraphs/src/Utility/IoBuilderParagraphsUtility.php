<?php

namespace Drupal\io_builder_paragraphs\Utility;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;
use Drupal\field\Entity\FieldConfig;
use Drupal\io_builder\Plugin\IoBuilder\Context\IoBuilderEntityContextInterface;
use Drupal\io_builder\Plugin\IoBuilder\EntityActions\BaseEntityActions;
use Drupal\io_builder\Plugin\IoBuilderContextPluginManager;
use Drupal\io_builder_paragraphs\Plugin\IoBuilder\Context\IoBuilderParagraphFieldContext;
use Drupal\paragraphs\Plugin\EntityReferenceSelection\ParagraphSelection;

/**
 * The IO builder paragraphs utility.
 *
 * @package Drupal\io_builder_paragraphs\Utility
 */
class IoBuilderParagraphsUtility {

  use StringTranslationTrait;

  /**
   * The selection plugin manager.
   *
   * @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface
   */
  protected SelectionPluginManagerInterface $selectionPluginManager;

  /**
   * The IO Builder context plugin manager.
   *
   * @var \Drupal\io_builder\Plugin\IoBuilderContextPluginManager
   */
  private IoBuilderContextPluginManager $ioBuilderContextPluginManager;

  /**
   * IoBuilderParagraphsUtility constructor.
   *
   * @param \Drupal\io_builder\Plugin\IoBuilderContextPluginManager $ioBuilderContextPluginManager
   *   The context plugin manager.
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selectionPluginManager
   *   The selection plugin manager.
   */
  public function __construct(
    IoBuilderContextPluginManager $ioBuilderContextPluginManager,
    SelectionPluginManagerInterface $selectionPluginManager,
  ) {
    $this->selectionPluginManager = $selectionPluginManager;
    $this->ioBuilderContextPluginManager = $ioBuilderContextPluginManager;
  }

  /**
   * Returns the paragraph options for a field of an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *   The entity for which to retrieve the paragraph options.
   * @param string $field
   *   The field.
   *
   * @return array<mixed>
   *   An array containing options.
   */
  public function getOptions(ContentEntityBase $entity, string $field): array {
    // Get field definition.
    $field_definition = $entity->getFieldDefinition($field);

    if (!$field_definition instanceof FieldConfig) {
      return [];
    }

    $handler = $this->selectionPluginManager->getSelectionHandler($field_definition);

    if (!$handler instanceof ParagraphSelection) {
      return [];
    }

    return $handler->getSortedAllowedTypes();
  }

  /**
   * Builds an add more button for paragraphs.
   *
   * @param \Drupal\io_builder\Plugin\IoBuilder\Context\IoBuilderEntityContextInterface $entityContext
   *   The entity context.s.
   * @param int|null $delta
   *   The delta of where to add the paragraph.
   *
   * @return array<mixed>
   *   A renderable array representing the add section.
   */
  public function buildAddMore(IoBuilderEntityContextInterface $entityContext, ?int $delta = NULL) {
    if (is_int($delta)) {
      $queryParams = [
        'delta' => $delta,
      ];
    }

    $link = BaseEntityActions::baseActionLink() + [
      '#key' => 'add',
      '#title' => $this->t('Add new paragraph'),
      '#url' => Url::fromRoute(
        'io_builder_paragraphs.add',
        $entityContext->getRouteParameters(),
        [
          'query' => $queryParams ?? [],
        ]
      ),
    ];

    $link['#attributes']['class'] = [
      'io-add-item',
      'has-tooltip',
    ];

    return [
      '#theme' => 'io_builder__add_section',
      '#link' => $link,
    ];
  }

  /**
   * Creates a delete link for a paragraph from a field.
   *
   * @param \Drupal\io_builder_paragraphs\Plugin\IoBuilder\Context\IoBuilderParagraphFieldContext $context
   *   The context of the paragraph and the field.
   *
   * @return array<mixed>
   *   The delete link.
   */
  public function createDeleteLink(IoBuilderParagraphFieldContext $context) {
    $url = Url::fromRoute(
      'io_builder_paragraphs.delete',
      $context->getRouteParameters(),
      [
        'query' => [
          'force' => TRUE,
        ],
      ]
    );

    return [
      '#theme' => 'io_builder__confirmation',
      '#title' => $this->t('Are you sure you wish to remove this paragraph?'),
      '#action' => [
        '#type' => 'link',
        '#attributes' => [
          'data-io-builder-action' => 'ajax_action',
        ],
        '#title' => $this->t('Remove paragraph'),
        '#url' => $url,
      ],
    ];
  }

  /**
   * Creates a delete link for a paragraph from a field.
   *
   * @param \Drupal\io_builder_paragraphs\Plugin\IoBuilder\Context\IoBuilderParagraphFieldContext $context
   *   The context of the paragraph and the field.
   * @param string $paragraphId
   *   The paragraph id.
   *
   * @return array<mixed>
   *   The delete link.
   */
  public function createPromoteLink(IoBuilderParagraphFieldContext $context, string $paragraphId) {
    $parameters = ['paragraph_id' => $paragraphId] + $context->getRouteParameters();

    $url = Url::fromRoute(
      'io_builder_paragraphs.promote',
      $parameters,
      [
        'query' => [
          'force' => TRUE,
        ],
      ]
    );

    return [
      '#theme' => 'io_builder__confirmation',
      '#action' => [
        '#type' => 'link',
        '#attributes' => [
          'data-io-builder-action' => 'ajax_action',
        ],
        '#title' => $this->t('Promote paragraph to library'),
        '#url' => $url,
      ],
    ];
  }

  /**
   * Creates a delete link for a paragraph from a field.
   *
   * @param \Drupal\io_builder_paragraphs\Plugin\IoBuilder\Context\IoBuilderParagraphFieldContext $context
   *   The context of the paragraph and the field.
   * @param string $paragraphId
   *   The paragraph id.
   *
   * @return array<mixed>
   *   The delete link.
   */
  public function createUnlinkLink(IoBuilderParagraphFieldContext $context, string $paragraphId) {
    $parameters = ['paragraph_id' => $paragraphId] + $context->getRouteParameters();

    $url = Url::fromRoute(
      'io_builder_paragraphs.unlink',
      $parameters,
      [
        'query' => [
          'force' => TRUE,
        ],
      ]
    );

    return [
      '#theme' => 'io_builder__confirmation',
      '#description' => $this->t('This will create a copy of the paragraph on the current page. It will not receive updates made to the original paragraph.'),
      '#action' => [
        '#type' => 'link',
        '#attributes' => [
          'data-io-builder-action' => 'ajax_action',
        ],
        '#title' => $this->t('Unlink paragraph from library'),
        '#url' => $url,
      ],
    ];
  }

  /**
   * Builds the paragraph selector.
   *
   * @param \Drupal\io_builder_paragraphs\Plugin\IoBuilder\Context\IoBuilderParagraphFieldContext $context
   *   The io builder paragraph field context.
   *
   * @return array<mixed>
   *   An array containing the paragraph selector.
   */
  public function buildParagraphSelector(IoBuilderParagraphFieldContext $context) {
    $options = $this->getOptions(
      $context->getEntity(),
      $context->getField()
    );

    if (empty($options)) {
      return [];
    }

    $links = [];
    $delta = $context->getDelta();

    if (is_int($delta)) {
      $queryParams['delta'] = $delta;
    }

    $build['#theme'] = 'io_builder_paragraphs__paragraphs_selector';

    foreach ($options as $key => $option) {
      $queryParams['type'] = $key;

      $links[$key] = [
        '#type' => 'link',
        '#attributes' => [
          'data-io-builder-action' => 'ajax_action',
        ],
        '#title' => $option['label'],
        '#weight' => $option['weight'] ?? 0,
        '#url' => Url::fromRoute(
          'io_builder_paragraphs.add', $context->getRouteParameters()
        ),
        '#options' => [
          'query' => $queryParams,
          'attributes' => [
            'class' => [
              'io-add-item',
              'has-tooltip',
            ],
            'data-tippy-content' => $this->t('Add paragraph'),
          ],
        ],
      ];
    }

    $build['#options'] = $links;
    return $build;
  }

  /**
   * Extends a raw array of paragraphs to use the IO Builder functionality.
   *
   * @param \Drupal\io_builder_paragraphs\Plugin\IoBuilder\Context\IoBuilderParagraphFieldContext $entityParagraphContext
   *   The entity field context.
   * @param array<mixed> $build
   *   The build array.
   */
  public function extendArray(IoBuilderParagraphFieldContext $entityParagraphContext, array &$build): void {
    // Get current field size.
    // @todo maybe change the field to an internal context function?
    $entity = $entityParagraphContext->getEntity();

    if (!$entity->hasField($entityParagraphContext->getField())) {
      return;
    }

    $field = $entity->get(
      $entityParagraphContext->getField()
    );

    if (!$field instanceof EntityReferenceRevisionsFieldItemList) {
      return;
    }

    // Get cardinality.
    // @todo make sure that cardinality is taken in to account!!!!!
    $size = $field->count();

    if ($size === 0) {
      $build[0] = [
        '#theme' => 'io_builder__placeholder',
        '#content' => $this->buildAddMore(
          $entityParagraphContext
        ),
      ];
      return;
    }

    // Add link to build array.
    for ($i = 0; $i < $size; $i++) {
      if (empty($build[$i]['#paragraph'])) {
        continue;
      }

      $paragraphContext = $this->ioBuilderContextPluginManager->createInstance(
        'io_builder_paragraph_field_context', $entityParagraphContext->getConfiguration()
      );

      $paragraphContext->setDelta($i);

      $build[$i]['#io_builder']['#paragraph_context'] = $paragraphContext;
      $build[$i]['#io_builder']['add_more_top'] = $this->buildAddMore($entityParagraphContext, $i) + [
        '#position' => 'top',
      ];
    }

    // Add a final placeholder to add paragraphs.
    $build[$size] = [
      '#theme' => 'io_builder__placeholder',
      '#content' => $this->buildAddMore($entityParagraphContext) + [
        '#position' => 'center',
      ],
    ];
  }

}
