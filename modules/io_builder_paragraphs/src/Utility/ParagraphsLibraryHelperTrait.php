<?php

namespace Drupal\io_builder_paragraphs\Utility;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\paragraphs\ParagraphInterface;

/**
 * A helper trait for handling the paragraphs library.
 */
trait ParagraphsLibraryHelperTrait {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface | NULL
   */
  protected $moduleHandler;

  /**
   * Returns the module handler.
   *
   * @return \Drupal\Core\Extension\ModuleHandlerInterface
   *   The module handler.
   */
  protected function getModuleHandler(): ModuleHandlerInterface {
    if (!$this->moduleHandler instanceof ModuleHandlerInterface) {
      $this->moduleHandler = \Drupal::service('module_handler');
    }
    return $this->moduleHandler;
  }

  /**
   * Checks if the paragraphs library module is enabled.
   *
   * @return bool
   *   TRUE if the paragraphs library module is enabled, FALSE otherwise.
   */
  protected function isParagraphsLibraryEnabled(): bool {
    return $this->getModuleHandler()->moduleExists('paragraphs_library');
  }

  /**
   * Checks if the paragraph can be promoted to the library.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   The paragraph to check.
   *
   * @return bool
   *   TRUE if the paragraph can be promoted to the library, FALSE otherwise.
   */
  protected function canPromoteToLibrary(ParagraphInterface $paragraph): bool {
    if ($this->isLibraryParagraph($paragraph)) {
      return FALSE;
    }
    return (bool) $paragraph->getParagraphType()->getThirdPartySetting('paragraphs_library', 'allow_library_conversion', FALSE);
  }

  /**
   * Checks if the paragraph is a library paragraph.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   The paragraph to check.
   *
   * @return bool
   *   TRUE if the paragraph is a library paragraph, FALSE otherwise.
   */
  protected function isLibraryParagraph(ParagraphInterface $paragraph): bool {
    return $paragraph->bundle() === 'from_library';
  }

}
