<?php

namespace Drupal\io_builder_paragraphs\Controller;

use Drupal\io_builder\Controller\IoBuilderController;
use Drupal\io_builder_paragraphs\Utility\IoBuilderParagraphsUtility;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Contains the functionality to add/edit/remove paragraphs using IO builder.
 *
 * @package Drupal\io_builder_paragraphs\Controller
 */
abstract class IoBuilderParagraphsControllerBase extends IoBuilderController {

  /**
   * The paragraphs utility.
   *
   * @var \Drupal\io_builder_paragraphs\Utility\IoBuilderParagraphsUtility
   */
  protected IoBuilderParagraphsUtility $paragraphsUtility;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $static = parent::create($container);

    $static->setParagraphsUtility(
      $container->get('io_builder_paragraphs.utility.paragraphs')
    );

    return $static;
  }

  /**
   * Sets the paragraphs utility.
   *
   * @param \Drupal\io_builder_paragraphs\Utility\IoBuilderParagraphsUtility $paragraphsUtility
   *   The paragraphs utility.
   */
  public function setParagraphsUtility(IoBuilderParagraphsUtility $paragraphsUtility): void {
    $this->paragraphsUtility = $paragraphsUtility;
  }

}
