<?php

namespace Drupal\io_builder_paragraphs\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\io_builder_paragraphs\Plugin\IoBuilder\Context\IoBuilderParagraphFieldContext;
use Drupal\io_builder_paragraphs\Utility\ParagraphsLibraryHelperTrait;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs_library\Entity\LibraryItem;
use Symfony\Component\HttpFoundation\Request;

/**
 * Contains the functionality to add/edit/remove paragraphs using IO builder.
 *
 * @package Drupal\io_builder_paragraphs\Controller
 */
class IoBuilderParagraphsLibraryController extends IoBuilderParagraphsControllerBase {

  use ParagraphsLibraryHelperTrait;

  /**
   * Promotes a paragraph to the library.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An ajax response.
   */
  public function promote(Request $request): AjaxResponse {
    if (!$this->isParagraphsLibraryEnabled()) {
      return $this->displayErrors([
        'The paragraphs library is not enabled',
      ]);
    }
    $force = $request->get('force') ?? NULL;
    $context = $this->getIoBuilderContextFromRequest($request);

    if (!$context instanceof IoBuilderParagraphFieldContext) {
      return $this->displayErrors([
        'The context as not of the IO builder pagraph field context type',
      ]);
    }

    $paragraphId = $request->get('paragraph_id') ?? NULL;

    if (!$force) {
      return $this->createModalResponse(
        $this->t('Are you sure you wish to promote this paragraph?'),
        $this->paragraphsUtility->createPromoteLink($context, $paragraphId)
      );
    }
    if (empty($paragraphId)) {
      return $this->displayErrors([
        'The paragraph id is missing',
      ]);
    }
    $paragraph = Paragraph::load($paragraphId);
    $libraryItem = LibraryItem::createFromParagraph($paragraph);
    $libraryItem->save();

    if (is_int($context->getDelta())) {
      $libraryParagraph = Paragraph::create([
        'type' => 'from_library',
        'field_reusable_paragraph' => $libraryItem,
      ]);
      $field = $context->getFieldList();
      $field->set($context->getDelta(), $libraryParagraph);
      $context->getEntity()->save();
    }

    return $this->rebuildFromContext($context);
  }

  /**
   * Unlinks a paragraph from the library.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An ajax response.
   */
  public function unlink(Request $request): AjaxResponse {
    if (!$this->isParagraphsLibraryEnabled()) {
      return $this->displayErrors([
        'The paragraphs library is not enabled',
      ]);
    }
    $force = $request->get('force') ?? NULL;
    $context = $this->getIoBuilderContextFromRequest($request);

    if (!$context instanceof IoBuilderParagraphFieldContext) {
      return $this->displayErrors([
        'The context as not of the IO builder pagraph field context type',
      ]);
    }

    $paragraphId = $request->get('paragraph_id') ?? NULL;

    if (!$force) {
      return $this->createModalResponse(
        $this->t('Are you sure you wish to unlink this paragraph from the library?'),
        $this->paragraphsUtility->createUnlinkLink($context, $paragraphId)
      );
    }

    if (empty($paragraphId)) {
      return $this->displayErrors([
        'The paragraph id is missing',
      ]);
    }

    $paragraph = Paragraph::load($paragraphId);
    $originalParagraph = NULL;
    if ($paragraph->hasField('field_reusable_paragraph')) {
      /** @var \Drupal\paragraphs_library\Entity\LibraryItem $libraryItem */
      $libraryItem = $paragraph->get('field_reusable_paragraph')->entity;
      if ($libraryItem instanceof LibraryItem) {
        $originalParagraph = $libraryItem->get('paragraphs')->entity;
      }
    }

    // Do nothing in case we fail to get the original paragraph.
    if (!$originalParagraph instanceof Paragraph) {
      return $this->rebuildFromContext($context);
    }

    $duplicateParagraph = $originalParagraph->createDuplicate();

    if (is_int($context->getDelta())) {
      $field = $context->getFieldList();
      $field->set($context->getDelta(), $duplicateParagraph);
      $context->getEntity()->save();
    }

    return $this->rebuildFromContext($context);
  }

}
