<?php

namespace Drupal\io_builder_paragraphs\Plugin\IoBuilder\EntityActions;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\io_builder\Plugin\IoBuilder\EntityActions\BaseEntityActions;
use Drupal\io_builder_paragraphs\Plugin\IoBuilder\Context\IoBuilderParagraphFieldContext;
use Drupal\io_builder_paragraphs\Utility\ParagraphsLibraryHelperTrait;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Contains IO Builder actions specific to paragraphs.
 *
 * @package Drupal\io_builder_paragraphs\Plugin\IoBuilder\EntityActions
 *
 * @IoBuilderEntityActions(
 *   id = "io_builder_paragraph_actions",
 *   label = @Translation("IO Builder Paragraph Actions"),
 *   entityTypes={"paragraph"}
 * )
 */
class ParagraphActions extends BaseEntityActions {

  use ParagraphsLibraryHelperTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getActions(array &$build = []): array {
    $paragraphContext = $build['#io_builder']['#paragraph_context'] ?? NULL;
    $links = [];
    $allow_promote_to_library = FALSE;
    $allow_unlink_from_library = FALSE;

    $paragraph = $build['#paragraph'] ?? NULL;

    if ($this->isParagraphsLibraryEnabled()) {
      if ($paragraph instanceof Paragraph) {
        $allow_promote_to_library = $this->canPromoteToLibrary($paragraph);
        $allow_unlink_from_library = $this->isLibraryParagraph($paragraph);
      }
    }

    if ($paragraphContext instanceof IoBuilderParagraphFieldContext) {
      if ($allow_promote_to_library) {
        $links['promote_to_library'] = $this->getPromoteToLibraryLink($paragraphContext, $paragraph->id());
      }
      if ($allow_unlink_from_library) {
        $links['unlink_from_library'] = $this->getUnlinkFromLibraryLink($paragraphContext, $paragraph->id());
      }
      $links['drag_drop'] = $this->getDragDropItemLink();
      $links['item_delete'] = $this->getDeleteItemLink($paragraphContext);
    }

    return $links;
  }

  /**
   * This creates a delete from item context.
   *
   * This won't delete the entity, only the item inside the list.
   *
   * @param \Drupal\io_builder_paragraphs\Plugin\IoBuilder\Context\IoBuilderParagraphFieldContext $paragraphContext
   *   The paragraph context.
   *
   * @return array<mixed>
   *   An array containing a entity action.
   */
  protected function getDeleteItemLink(IoBuilderParagraphFieldContext $paragraphContext): array {
    return BaseEntityActions::baseActionLink() + [
      '#title' => $this->t('Delete'),
      '#url' => Url::fromRoute(
          'io_builder_paragraphs.delete',
          $paragraphContext->getRouteParameters()
      ),
      '#key' => 'delete',
    ];
  }

  /**
   * Gets the drag & drop item link.
   *
   * @return array<mixed>
   *   An array containing the drag & drop functionality.
   */
  protected function getDragDropItemLink(): array {
    return [
      '#theme' => 'io_builder__action',
      '#title' => $this->t('Move'),
      '#key' => 'move',
      '#attached' => [
        'library' => [
          'io_builder/io_builder_move',
        ],
      ],
    ];
  }

  /**
   * Gets the drag & drop item link.
   *
   * @param \Drupal\io_builder_paragraphs\Plugin\IoBuilder\Context\IoBuilderParagraphFieldContext $paragraphContext
   *   The paragraph context.
   * @param string $paragraphId
   *   The paragraph id.
   *
   * @return array<mixed>
   *   An array containing the drag & drop functionality.
   */
  protected function getPromoteToLibraryLink(IoBuilderParagraphFieldContext $paragraphContext, string $paragraphId): array {
    $parameters = ['paragraph_id' => $paragraphId] + $paragraphContext->getRouteParameters();

    return BaseEntityActions::baseActionLink() + [
      '#title' => $this->t('Promote to library'),
      '#key' => 'promote',
      '#url' => Url::fromRoute(
          'io_builder_paragraphs.promote',
          $parameters
      ),
    ];
  }

  /**
   * Gets the drag & drop item link.
   *
   * @param \Drupal\io_builder_paragraphs\Plugin\IoBuilder\Context\IoBuilderParagraphFieldContext $paragraphContext
   *   The paragraph context.
   * @param string $paragraphId
   *   The paragraph id.
   *
   * @return array<mixed>
   *   An array containing the drag & drop functionality.
   */
  protected function getUnlinkFromLibraryLink(IoBuilderParagraphFieldContext $paragraphContext, string $paragraphId): array {
    $parameters = ['paragraph_id' => $paragraphId] + $paragraphContext->getRouteParameters();

    return BaseEntityActions::baseActionLink() + [
      '#title' => $this->t('Unlink from library'),
      '#key' => 'unlink',
      '#url' => Url::fromRoute(
          'io_builder_paragraphs.unlink',
          $parameters
      ),
    ];
  }

}
