<?php

namespace Drupal\io_builder_paragraphs\Plugin\IoBuilder\Field;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\io_builder\Plugin\IoBuilder\Context\IoBuilderEntityContextInterface;
use Drupal\io_builder\Plugin\IoBuilder\Context\IoBuilderEntityFieldContext;
use Drupal\io_builder\Plugin\IoBuilder\Field\IoBuilderFieldBase;
use Drupal\io_builder\Plugin\IoBuilderContextPluginManager;
use Drupal\io_builder_paragraphs\Utility\IoBuilderParagraphsUtility;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds IO builder functionality to paragraph fields.
 *
 * @package Drupal\io_builder_paragraphs\Plugin\IoBuilder\Field
 *
 * @IoBuilderField(
 *   id = "io_builder_paragraphs",
 *   label = @Translation ("Io Builder Paragraphs"),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class IoBuilderParagraphsField extends IoBuilderFieldBase {

  use StringTranslationTrait;

  /**
   * This utility contains functions that we use accross multiple classes.
   *
   * @var \Drupal\io_builder_paragraphs\Utility\IoBuilderParagraphsUtility
   */
  protected IoBuilderParagraphsUtility $utility;
  /**
   * The context plugin manager is used to create a paragraph context.
   *
   * @var \Drupal\io_builder\Plugin\IoBuilderContextPluginManager
   */
  protected IoBuilderContextPluginManager $contextPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $static = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $static->setUtility(
      $container->get('io_builder_paragraphs.utility.paragraphs')
    );

    $static->setContextPluginManager(
      $container->get('plugin.manager.io_builder_context')
    );

    return $static;
  }

  /**
   * {@inheritdoc}
   *
   * Converting the context to an IO Builder paragraph context here.
   */
  public function setContext(IoBuilderEntityContextInterface $context): void {
    $this->context = $this->contextPluginManager->createInstance(
      'io_builder_paragraph_field_context', $context->getConfiguration()
    );
  }

  /**
   * Adds additional IO builder functionality to a paragraphs field.
   */
  public function alterBuild(&$build): void {
    if (!$this->context instanceof IoBuilderEntityFieldContext) {
      return;
    }

    $this->utility->extendArray($this->context, $build);
  }

  /**
   * Sets the paragraphs utility.
   *
   * @param \Drupal\io_builder_paragraphs\Utility\IoBuilderParagraphsUtility $utility
   *   The utility class.
   */
  public function setUtility(IoBuilderParagraphsUtility $utility): void {
    $this->utility = $utility;
  }

  /**
   * Sets the context plugin manager.
   *
   * @param \Drupal\io_builder\Plugin\IoBuilderContextPluginManager $contextPluginManager
   *   The context plugin manager.
   */
  public function setContextPluginManager(IoBuilderContextPluginManager $contextPluginManager): void {
    $this->contextPluginManager = $contextPluginManager;
  }

}
