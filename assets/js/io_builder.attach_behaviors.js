((Drupal) => {
  Drupal.AjaxCommands.prototype.ioBuilderAttachBehaviors = (
    ajax,
    response,
    status,
  ) => {
    Drupal.attachBehaviors();
  };
})(Drupal);
