(($, Drupal, drupalSettings) => {
  Drupal.behaviors.ioBuilderTooltip = {
    attach: (context, settings) => {
      /* eslint-disable-next-line no-undef */
      tippy('.has-tooltip', {
        theme: 'io-builder',
        animation: 'shift-away-subtle',
      });
    },
  };
})(jQuery, Drupal, drupalSettings);
