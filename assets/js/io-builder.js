(($, Drupal, drupalSettings, once) => {
  Drupal.behaviors.ioBuilder = {
    /**
     * {@inheritdoc}
     */
    attach: function (context, settings) {
      this.attachPanelListeners(context, settings);
      this.attachActionListeners(context, settings);
    },

    /**
     * Attaches event listeners to the IO builder panels.
     *
     * @param context
     *   The context
     * @param settings
     *   The settings
     */
    attachPanelListeners: function (context, settings) {
      let $panel = $('body').find('#io-builder-panel');

      if ($panel.length === 0 && $(context).hasClass('io-builder-panel')) {
        $panel = $(context);
      }

      $(once('panel_init', $panel));

      $panel.find('[data-close], [data-overlay]').on('click', function () {
        $(this).closest('.io-builder-panel').remove();
      });
    },

    /**
     * Attaches event listeners to the IO actions.
     *
     * @param context
     *   The context
     * @param settings
     *   The settings
     */
    attachActionListeners: function (context, settings) {
      const self = this;
      const $actions = $(
        once(
          'ajax_action_init',
          $('body').find('a[data-io-builder-action="ajax_action"]'),
        ),
      );

      $actions.on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass('io-builder-action--edit')) {
          $(this).addClass('is-active');
        }

        const $element = $(this).parents('[data-io-builder-element]').first();

        const loadAjax = Drupal.ajax({
          url: $(this).attr('href'),
          type: 'POST',
          submit: {
            io_builder_context_tree: self.retrieveContextData($element),
          },
        });

        loadAjax.execute().done(() => {
          Drupal.attachBehaviors();
          $actions.removeClass('is-active');
        });
      });
    },

    /**
     * Retrieves the context data for an element.
     *
     * @param $element
     *   The element.
     *
     * @returns {{parent: ({}|*), entity: ({}|*)}}
     *   An array which is sent with the requests.
     */
    retrieveContextData: function ($element) {
      return {
        parent: this.getParentData($element),
        top_parent: this.getTopParentData($element),
        entity: this.getIoBuilderData($element),
      };
    },

    /**
     * Gets last the parent IO builder data from an element.
     *
     * The parent is the top IO builder element.
     *
     * @param $element
     *   The element.
     *
     * @returns {{}|*}
     *   Contains the io builder data.
     */
    getTopParentData: function ($element) {
      const $parent = $element.parents('[data-io-builder-element]').last();

      if ($parent.length <= 0) {
        return {};
      }

      return this.getIoBuilderData($parent);
    },

    /**
     * Gets the parent IO builder data from an element.
     *
     * The parent is the top IO builder element.
     *
     * @param $element
     *   The element.
     *
     * @returns {{}|*}
     *   Contains the io builder data.
     */
    getParentData: function ($element) {
      const $parent = $element.parents('[data-io-builder-element]').first();

      if ($parent.length <= 0) {
        return {};
      }

      return this.getIoBuilderData($parent);
    },

    /**
     * Gets the IO Builder data from the element.
     *
     * @param $element
     *   The element.
     *
     * @returns {{}|*}
     *   Contains the io builder data.
     */
    getIoBuilderData: function ($element) {
      const ioBuilderData = $element.data('io-builder-data');

      if (!ioBuilderData) {
        return {};
      }

      return ioBuilderData;
    },
  };
})(jQuery, Drupal, drupalSettings, once);
