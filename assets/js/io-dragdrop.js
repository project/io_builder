(($, Drupal, drupalSettings, Sortable) => {
  Drupal.behaviors.ioBuilderDragDrop = {
    attach: (context, settings) => {
      const elements = document.querySelectorAll('.io-builder--sort-wrapper');

      if (elements) {
        elements.forEach((el) => {
          Sortable.create(el, {
            animation: 150,
            ghostClass: 'drag-highlighted-ghost',
            swapThreshold: 0.72,
            handle: '.io-builder-action--move',
            onEnd: (evt) => {
              const $parent = $(evt.from);

              if ($parent.length < 0) {
                return;
              }

              const parentData =
                Drupal.behaviors.ioBuilder.getParentData($parent);

              if (!parentData) {
                return;
              }

              const field = $parent.data('io-builder-field');

              const indexSwitch = {
                from: evt.oldIndex,
                to: evt.newIndex,
              };

              const loadAjax = Drupal.ajax({
                url: Drupal.url('io-builder/drag-drop'),
                type: 'POST',
                submit: {
                  io_builder_context_tree: {
                    parent: parentData,
                  },
                  field,
                  index_switch: indexSwitch,
                },
              });

              loadAjax.execute().done(() => {
                Drupal.attachBehaviors();
              });
            },
          });
        });
      }
    },
  };
})(jQuery, Drupal, drupalSettings, Sortable);
