# IO Builder (IOB)
## Contributing to the IOB assets code
IOB is currently using the Intracto Buildozer build system for more efficient
development. If you want to contribute to the IOB code you
can check out the Intracto Buildozer docs here:
[Explore Intracto Buildozer docs »](https://github.com/Intracto/buildozer)

## Quick start
```bash
# Install dependencies
yarn install
# Build assets
yarn build
# Watch files
yarn watch
```

More information about the [IO Builder](https://www.drupal.org/project/issues/io_builder).

## Creators
**Maarten Heip**
- <https://www.drupal.org/u/mheip>

**Jerôme Schelkens**
- <https://www.drupal.org/u/JeromeS>

## Thanks
A special thanks to Intracto, for allowing us to do this on company time.
https://www.intracto.com
