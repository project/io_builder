/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "../../templates/**/*.{twig,inc,php,css,js}",
    "../js/**/*.{ts,js}",
    "../../src/**/*.{twig,inc,php,css,js}",
    "../../modules/**/*.{twig,inc,php,css,js}",
  ],
  safelist: [
    'io-add-item',
    'iob-btn',
    'ui-dialog',
    'drag-highlighted-ghost'
  ],
  prefix: 'iob-',
  theme: {
    fontFamily: {
      'display': '"Montserrat", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
      'body': '"Roboto", -apple-system, system-ui, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"'
    },
    extend: {
      colors: {
        primary: {
          DEFAULT: '#0017EE',
          100: '#5B83DB',
        },
        secondary: {
          DEFAULT: '#BDCAD1',
          100: '#DCE1E5',
          700: '#82a9be',
        },
        dark: '#000000',
        pink: {
          DEFAULT: '#DCC8C2',
          100: '#EBE5E3',
        },
        purple: {
          DEFAULT: '#863170',
          100: '#BAA9B9',
          600: '#5E2D5E',
        },
        green: {
          DEFAULT: '#139678',
          100: '#97BAB2',
          700: '#128159',
        },
        warning: {
          DEFAULT: '#d37a54',
          100: '#cfb19e',
        },
        danger: {
          DEFAULT: '#D35454',
          100: '#CFAA9E',
        },
        info: '#5B83DB',
      },
      keyframes: {
        'fade-in': {
          '0%': {
            opacity: 0,
          },
          '100%': {
            opacity: 1,
          },
        },
        'fade-in-up': {
          '0%': {
            opacity: 0,
            transform: 'translateY(20px)',
          },
          '100%': {
            opacity: 1,
            transform: 'translateY(0)',
          },
        },
        'slide-in-l': {
          '0%': {transform: 'translateX(-100%)'},
          '100%': {transform: 'translateX(0)'},
        },
        'fade-in-down': {
          '0%': {
            opacity: 0,
            transform: 'translateY(-10px)',
          },
          '100%': {
            opacity: 1,
            transform: 'translateY(0)',
          },
        },
      },
      animation: {
        'fade-in': 'fade-in .3s both',
        'fade-in-up': 'fade-in-up .3s both',
        'fade-in-down': 'fade-in-down .3s both',
        'slide-in-l': 'slide-in-l .3s both',
      },
      zIndex: {
        '1': '1',
        '100': '100',
        '1000': '1000',
      },
    },
  },
  important: true,
  plugins: [require('@tailwindcss/forms')],
};
