<?php

namespace Drupal\io_builder\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * The IO Builder context.
 *
 * @package Drupal\io_builder\Annotation
 *
 * @Annotation
 */
class IoBuilderContext extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

}
