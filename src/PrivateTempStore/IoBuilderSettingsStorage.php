<?php

namespace Drupal\io_builder\PrivateTempStore;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * Contains settings for the current user.
 *
 * @package Drupal\io_builder\PrivateTempStore
 */
class IoBuilderSettingsStorage extends IoBuilderStorage {

  /**
   * IoBuilderSettingsStorage constructor.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $privateTempStorageFactory
   *   The private temp store factory service.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   */
  public function __construct(
    PrivateTempStoreFactory $privateTempStorageFactory,
    protected AccountProxyInterface $currentUser,
  ) {
    parent::__construct($privateTempStorageFactory);
    $this->initialiseTempStore('io_builder_settings');
  }

  /**
   * Toggles the IO builder, if enabled entities can be adjusted.
   */
  public function toggleIoBuilder() {
    $reverse = !$this->ioBuilderEnabled();
    $this->tempStore->set('enabled', $reverse);
  }

  /**
   * Is the IO builder enabled?
   *
   * @return bool
   *   Tells us whether or not the IO builder has been enabled.
   */
  public function ioBuilderEnabled(): bool {
    if ($this->currentUser->isAnonymous()) {
      return FALSE;
    }

    try {
      $ioBuilderEnabled = $this->tempStore->get('enabled') ?? FALSE;
    }
    catch (\Exception $e) {
      return FALSE;
    }

    return (bool) $ioBuilderEnabled;
  }

}
