<?php

namespace Drupal\io_builder\Exception;

/**
 * Exception.
 */
class EntityStorageSetupException extends \Exception {
}
