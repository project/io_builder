<?php

namespace Drupal\io_builder\Command;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Helper command to re-attach behaviors.
 *
 * @package Drupal\io_builder\Command
 */
class AttachBehaviorsCommand implements CommandInterface {

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'ioBuilderAttachBehaviors',
      'data' => [],
    ];
  }

}
