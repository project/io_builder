<?php

namespace Drupal\io_builder\Controller;

use Drupal\io_builder\Plugin\IoBuilder\Context\IoBuilderEntityFieldContext;
use Symfony\Component\HttpFoundation\Request;

/**
 * The IO Builder field controller.
 *
 * @package Drupal\io_builder\Controller
 */
class IoBuilderFieldController extends IoBuilderController {

  /**
   * Generates a field widget for an entity.
   */
  public function fieldWidget(Request $request) {
    $context = $this->initEntityContextFromRequest($request);

    if (!$context instanceof IoBuilderEntityFieldContext) {
      return $this->displayErrors('Could not retrieve field context');
    }

    // Prepare the form in the entity form builder utility.
    $this->entityFormBuilderUtility
      ->setContext($context)
      ->prepareEntityForm([
        'io_builder_context_tree' => $this->getIoBuilderContextTreeFromRequest($request),
        'allowed_fields' => [
          $context->getField(),
        ],
      ]);

    if ($this->entityFormBuilderUtility->isExecuted()) {
      return $this->rebuildFromContext(
        $this->entityFormBuilderUtility->getRebuildContext()
      );
    }
    elseif ($errors = $this->entityFormBuilderUtility->getErrors()) {
      return $this->displayErrors($errors);
    }

    return $this->displayIoBuilderPanelCommand(
      $this->entityFormBuilderUtility->getForm()
    );
  }

}
