<?php

namespace Drupal\io_builder\Plugin\IoBuilder\Field;

use Drupal\Core\Url;
use Drupal\io_builder\Plugin\IoBuilder\EntityActions\BaseEntityActions;

/**
 * Adds an action to fields to edit that single instance.
 *
 * @package Drupal\io_builder\Plugin\IoBuilder\Field
 *
 * @IoBuilderField(
 *   id = "edit_field",
 *   label = @Translation("Edit Field"),
 *   field_types = {}
 * )
 */
class EditField extends IoBuilderFieldBase {

  /**
   * {@inheritdoc}
   */
  public function alterBuild(&$build): void {
    $build['#attributes']['class'][] = 'io-builder--wrapper';

    if (!empty($build[0])) {
      $build['#io_builder'][] = [
        '#theme' => 'io_builder__actions',
        '#actions' => [
          'edit' => $this->getEditFieldLink('edit'),
        ],
      ];
    }
    else {
      $build[0] = [
        '#theme' => 'io_builder__placeholder',
        '#content' => [
          '#theme' => 'io_builder__add_section',
          '#link' => $this->getEditFieldLink(),
        ],
        '#attributes' => $this->context->getIoBuilderAttributes(),
      ];
    }
  }

  /**
   * Builds the edit field link.
   *
   * @param string $key
   *   The key.
   *
   * @return array
   *   The edit field link.
   */
  protected function getEditFieldLink(string $key = 'add'): array {
    $link = BaseEntityActions::baseActionLink() + [
      '#key' => $key,
      '#title' => $this->t('Edit field'),
      '#url' => Url::fromRoute(
        'io_builder.field_widget',
        $this->context->getRouteParameters(),
      ),
    ];

    $link['#attributes']['class'] = [
      'io-add-item',
      'has-tooltip',
    ];

    return $link;
  }

}
