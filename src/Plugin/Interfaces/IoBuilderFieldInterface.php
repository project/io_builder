<?php

namespace Drupal\io_builder\Plugin\Interfaces;

/**
 * Interface for IO Builder fields.
 *
 * @package Drupal\io_builder\Plugin\Interfaces
 */
interface IoBuilderFieldInterface {

  /**
   * Alters the build.
   *
   * @param array $build
   *   The build.
   */
  public function alterBuild(array &$build): void;

}
