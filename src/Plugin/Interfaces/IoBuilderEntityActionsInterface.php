<?php

namespace Drupal\io_builder\Plugin\Interfaces;

use Drupal\io_builder\Traits\IoBuilderEntityContextSetterInterface;

/**
 * The IO Builder entity actions interface.
 */
interface IoBuilderEntityActionsInterface extends IoBuilderEntityContextSetterInterface {

  /**
   * Returns an array of actions for a certain entity.
   *
   * @param array<mixed> $build
   *   An optional build array.
   *
   * @return array<mixed>
   *   An array of actions.
   */
  public function getActions(array &$build = []): array;

}
