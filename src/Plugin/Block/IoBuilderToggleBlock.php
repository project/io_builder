<?php

namespace Drupal\io_builder\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\io_builder\PrivateTempStore\IoBuilderSettingsStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class IoBuilderEnableBlock.
 *
 * @package Drupal\io_builder\Plugin\Block
 *
 * @Block(
 *   id = "io_builder_toggle_block",
 *   admin_label=@Translation("Io Builder - Toggle link"),
 *   category="io_builder"
 * )
 */
class IoBuilderToggleBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $accountProxy;

  /**
   * The IO Builder settings storage.
   *
   * @var \Drupal\io_builder\PrivateTempStore\IoBuilderSettingsStorage
   */
  protected IoBuilderSettingsStorage $settingsStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $static = new static(
      $configuration, $plugin_id, $plugin_definition
    );

    $static->accountProxy = $container->get('current_user');
    $static->settingsStorage = $container->get('io_builder.temp_store.settings');
    return $static;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (!$this->accountProxy->hasPermission('access io builder')) {
      return [];
    }

    $destination = Url::fromRoute('<current>')
      ->toString();

    $ioBuilderToggleLink = Url::fromRoute(
      'io_builder.toggle',
      ['destination' => $destination]
    );

    return [
      '#theme' => 'io_builder__toggle_link',
      '#url' => $ioBuilderToggleLink,
      '#is_enabled' => $this->settingsStorage->ioBuilderEnabled(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(
      parent::getCacheContexts(),
      ['user.permissions', 'io_builder_enabled']
    );
  }

}
