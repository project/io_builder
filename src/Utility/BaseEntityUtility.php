<?php

namespace Drupal\io_builder\Utility;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * The abstract BaseEntityUtility class.
 *
 * @package Drupal\io_builder\Utility
 */
abstract class BaseEntityUtility {

  use StringTranslationTrait;

  /**
   * The content entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityBase|null
   */
  protected ?ContentEntityBase $entity;

  /**
   * The view mode of the entity.
   *
   * @var string
   */
  protected string $viewMode;

  /**
   * Sets the entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase|null $entity
   *   The entity to set.
   */
  public function setEntity(?ContentEntityBase $entity): void {
    $this->entity = $entity;
  }

  /**
   * Sets the entity view mode.
   *
   * @param string $viewMode
   *   The view mode.
   */
  public function setViewMode(string $viewMode): void {
    $this->viewMode = $viewMode;
  }

}
