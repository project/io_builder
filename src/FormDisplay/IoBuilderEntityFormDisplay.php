<?php

namespace Drupal\io_builder\FormDisplay;

use Drupal\Core\Entity\Entity\EntityFormDisplay;

/**
 * The IO Builder entity form display class.
 */
class IoBuilderEntityFormDisplay extends EntityFormDisplay {

}
